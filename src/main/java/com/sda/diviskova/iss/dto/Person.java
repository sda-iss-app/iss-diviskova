package com.sda.diviskova.iss.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor

public class Person {

    @JsonProperty("name")
    private String name;
    private String craft;

}
