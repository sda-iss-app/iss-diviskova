package com.sda.diviskova.iss.dto;

import lombok.Data;

@Data
public class Cords {
    private Float longitude;
    public Float latitude;
}
