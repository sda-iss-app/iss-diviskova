package com.sda.diviskova.iss.dto;

import lombok.Data;

import java.util.List;


@Data
public class People {

   private List<Person> people;
   private Integer number;
   private String message;
}
