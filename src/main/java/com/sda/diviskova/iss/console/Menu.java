package com.sda.diviskova.iss.console;

import com.sda.diviskova.iss.Application;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Menu {
    private static final Logger LOGGER = LoggerFactory.getLogger(Menu.class);

    public static void printMainMenu () {
        var stringBuilder = new StringBuilder();
        stringBuilder.append("\n-----Main menu-----\n");
        stringBuilder.append("(1) Show current ISS position\n");
        stringBuilder.append("(2) Calculate ISS speed\n");
        stringBuilder.append("(3) List of people in the space\n");
        stringBuilder.append("(0) Exit application\n");

        LOGGER.info("{}",stringBuilder);
    }
}
