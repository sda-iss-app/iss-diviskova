package com.sda.diviskova.iss;

import com.sda.diviskova.iss.console.Main;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Application {

    private static final Logger LOGGER = LoggerFactory.getLogger(Application.class);

    public static void main (String [] args) {

        LOGGER.info("Starting ISS APP...");
        // urovne logovani:trace, debug,(kdyz nekde testujeme, ale v produkci je vypnuto), info, warn, error
        Main.run();
    }
}
