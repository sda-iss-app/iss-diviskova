package com.sda.diviskova.iss.controller;

import com.sda.diviskova.iss.dto.Person;
import com.sda.diviskova.iss.dto.Position;
import com.sda.diviskova.iss.service.IssService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.List;

public class ConsoleController {

    private final IssService issService;

    public ConsoleController(IssService issService) {
        this.issService = issService;
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(ConsoleController.class);


    public Position getCurrentISSPosition() {
    LOGGER.info("Showing current ISS position...");
    return issService.getIssPosition();
    }

    public Double getISSSpeed() {
        LOGGER.info("Get current ISS speed...");
        return 0d;
    }

    public List<Person> getListOfPeople(){
        LOGGER.info("Showing Astronauts...");
        return issService.getAstronauts();
    }

}
